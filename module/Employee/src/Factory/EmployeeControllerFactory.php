<?php

namespace Employee\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Employee\Controller\EmployeeController;
use Employee\Model\EmployeeModel;

class EmployeeControllerFactory implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {
        return new EmployeeController(
            $container->get(EmployeeModel::class)
        );
    }
}
