<?php

namespace Employee\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Employee\Model\EmployeeModel;
use RuntimeException;

class EmployeeController extends AbstractActionController
{
    private $employeeModel;

    public function __construct(EmployeeModel $employeeModel)
    {
        $this->employeeModel = $employeeModel;
    }

    /**
     * Default function to run when the route is accessed without additional segments
     *
     * @return string (JSON)
     */
    public function indexAction()
    {
        echo json_encode($this->employeeModel->getAll());
        exit;
    }

    /**
     * Function for creating new employee record.
     *
     * @return string (JSON)
     */
    public function addAction()
    {
        $request = $this->getRequest();

        // Convert request data into array
        parse_str($request->getContent(), $data);

        try {
            $newEmployee = $this->employeeModel->create($data);

            echo json_encode($newEmployee);
            exit;
        } catch (\InvalidArgumentException $iae) {
            http_response_code(400);
            echo json_encode(['error' => $iae->getMessage()]);
            exit;
        } catch (RuntimeException $re) {
            http_response_code(500);
            echo json_encode(['error' => 'Something went wrong.']);
            exit;
        }
    }

    /**
     * Function for editing existing employee record.
     *
     * @return string (JSON)
     */
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        $request = $this->getRequest();

        // Convert request data into array
        parse_str($request->getContent(), $data);

        try {
            if (empty($id)) {
                throw new \InvalidArgumentException('EmployeeID is required');
            }

            $updatedEmployee = $this->employeeModel->update($id, $data);

            echo json_encode($updatedEmployee);
            exit;
        } catch (\InvalidArgumentException $iae) {
            http_response_code(400);
            echo json_encode(['error' => $iae->getMessage()]);
            exit;
        } catch (RuntimeException $re) {
            http_response_code(500);
            echo json_encode(['error' => 'Something went wrong.']);
            exit;
        }
    }

    /**
     * Function to "delete" an existing employee.
     *
     * Note: since we do not really want to delete records, this function just changes
     *       the employee's employment status to "Terminated"
     *
     * @return string (JSON)
     */
    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        try {
            if (empty($id)) {
                throw new \InvalidArgumentException('EmployeeID is required');
            }

            $deletedEmployee = $this->employeeModel->delete($id);

            echo json_encode($deletedEmployee);
            exit;
        } catch (\InvalidArgumentException $iae) {
            http_response_code(400);
            echo json_encode(['error' => $iae->getMessage()]);
            exit;
        } catch (RuntimeException $re) {
            http_response_code(500);
            echo json_encode(['error' => 'Something went wrong.']);
            exit;
        }
    }
}
