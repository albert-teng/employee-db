<?php

namespace Employee\Model;

use DateTime;
use RuntimeException;
use Laminas\Db\Adapter\Adapter;

class EmployeeModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Adapter([
            'driver' => 'Pdo_Sqlite',
            'database' => 'data/employee.db'
        ]);
    }

    /**
     * Create an employee record using the provided data.
     *
     * @param array $data Array of data for the new employee
     *
     * @throws InvalidArgumentException
     */
    public function create(array $data)
    {
        $this->validateEmployeeData($data);

        $result = $this->db->query('
            INSERT INTO Employee
                (
                    `FirstName`,
                    `LastName`,
                    `BirthDate`,
                    `StartDate`,
                    `Position`,
                    `Department`
                )
            VALUES
                (?, ?, ?, ?, ?, ?)
            ;
        ', [
            $data['FirstName'],
            $data['LastName'],
            $data['BirthDate'],
            $data['StartDate'],
            $data['Position'],
            $data['Department']
        ]);

        if (! $result) {
            throw new RuntimeException('Unable to create new employee.');
        }

        $newEmployeeID = $result->getGeneratedValue();

        return $this->get($newEmployeeID);
    }

    /**
     * Mark an employee as "Deleted" or "Terminated"
     *
     * Note: This is basically just an update call to change the employee status
     *
     * @param int $id Employee ID
     *
     * @throws InvalidArgumentException
     *
     * @return boolean
     */
    public function delete(int $id)
    {
        $this->validateEmployeeData(['ID' => $id]);

        $employee = (array) $this->get($id);
        $employee['Status'] = 'Terminated';

        $this->update($id, $employee);

        return $employee;
    }

    /**
     * Retrieve all employee records
     *
     * @return array
     */
    public function getAll()
    {
        $query = $this->db->query('
            SELECT
                ID,
                FirstName,
                LastName,
                BirthDate,
                StartDate,
                Position,
                Department,
                EmploymentStatus AS Status
            FROM
                Employee
            ;
        ');

        $rows = $query->execute();

        // Convert ResultSet object into associative array so we can return it as JSON.
        foreach ($rows as $row) {
            $results[] = $row;
        }

        return $results;
    }

    /**
     * Retrieve an employee but their ID.
     *
     * @param int $id Employee ID
     *
     * @return array
     */
    public function get(int $id)
    {
        $result = $this->db->query('
            SELECT
                ID,
                FirstName,
                LastName,
                BirthDate,
                StartDate,
                Position,
                Department,
                EmploymentStatus AS Status
            FROM
                Employee
            WHERE
                ID = ?
            ;
        ', [$id]);

        // Return empty array if no employee found.
        if (! $result) {
            return [];
        }

        return $result->current();
    }

    /**
     * Update employee details
     *
     * @param int   $id   Employee ID to be updated.
     * @param array $data Updated data for the particular employee.
     *
     * @throws InvalidArgumentException
     *
     * @return array
     */
    public function update(int $id, array $data)
    {
        $this->validateEmployeeData($data);

        $result = $this->db->query('
            UPDATE
                Employee
            SET
                FirstName = ?,
                LastName = ?,
                BirthDate = ?,
                StartDate = ?,
                Position = ?,
                Department = ?,
                EmploymentStatus = ?
            WHERE
                ID = ?
            ;
        ', [
            $data['FirstName'],
            $data['LastName'],
            $data['BirthDate'],
            $data['StartDate'],
            $data['Position'],
            $data['Department'],
            $data['Status'],
            $id
        ]);

        if (! $result) {
            throw new RuntimeException('Unable to update employee data');
        }

        return $this->get($id);
    }

    /**
     * Helper function to validate incoming employee data.
     *
     * @param array $data Array of data containing employee data.
     *
     * @throws InvalidArgumentException
     *
     * @return boolean
     */
    private function validateEmployeeData(array $data)
    {
        $error = [];

        foreach ($data as $dataKey => $value) {
            switch ($dataKey) {
                case 'ID':
                    if (empty($value)) {
                        $error[] = 'ID is required.';
                    } elseif (! is_numeric($value)) {
                        $error[] = 'ID must be numeric value.';
                    } else {
                        $employee = $this->get($value);

                        if (empty($employee)) {
                            $error[] = 'Employee does not exists.';
                        }
                    }
                    break;

                case 'FirstName':
                    if (empty($value)) {
                        $error[] = 'First Name is required.';
                    }
                    break;

                case 'LastName':
                    if (empty($value)) {
                        $error[] = 'Last Name is required.';
                    }
                    break;

                case 'BirthDate':
                    if (empty($value)) {
                        $error[] = 'Date of Birth is required.';
                    } else {
                        $formattedDate = DateTime::createFromFormat('Y-m-d', $value);

                        if ($formattedDate && $formattedDate->format('Y-m-d') != $value) {
                            $error[] = 'Invalid date of birth provided.';
                        }
                    }
                    break;

                case 'StartDate':
                    if (empty($value)) {
                        $error[] = 'Start Date is required.';
                    } else {
                        $formattedDate = DateTime::createFromFormat('Y-m-d', $value);

                        if ($formattedDate && $formattedDate->format('Y-m-d') != $value) {
                            $error[] = 'Invalid start date provided.';
                        }
                    }
                    break;

                case 'Position':
                    if (empty($value)) {
                        $error[] = 'Position is required.';
                    }
                    break;

                case 'Department':
                    if (empty($value)) {
                        $error[] = 'Department is required.';
                    }
                    break;

                case 'Status':
                    if (empty($value)) {
                        $error[] = 'Status is required.';
                    }
                    break;
            }
        }

        if (! empty($error)) {
            $msg = implode('<br>', $error);

            throw new \InvalidArgumentException($msg);
        }

        return true;
    }
}
