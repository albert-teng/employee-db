<?php

namespace Employee;

use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\EmployeeController::class => Factory\EmployeeControllerFactory::class
        ]
    ],
    'router' => [
        'routes' => [
            'employee' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/employee[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+'
                    ],
                    'defaults' => [
                        'controller' => Controller\EmployeeController::class,
                        'action' => 'index'
                    ]
                ]
            ]
        ]
    ],
    'service_manager' => [
        'factories' => [
            Model\EmployeeModel::class => InvokableFactory::class
        ]
    ]
];
