-- Employee Table
CREATE TABLE IF NOT EXISTS `Employee` (
    `ID` integer PRIMARY KEY,
    `FirstName` varchar(30) NOT NULL,
    `LastName` varchar(30) NOT NULL,
    `BirthDate` date NOT NULL,
    `StartDate` date NOT NULL,
    `Position` text DEFAULT NULL,
    `Department` text DEFAULT NULL,
    `EmploymentStatus` text NOT NULL DEFAULT "Active",
    `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Reset table content
DELETE FROM `Employee`;

-- Seeding the table
INSERT INTO `Employee`
    (`FirstName`, `LastName`, `BirthDate`, `StartDate`, `Position`, `Department`)
VALUES
    ("Joe", "Cameron", "1983-12-02", "2001-03-14", "Sales", "Marketing"),
    ("Michael", "Boone", "1963-02-17", "2000-07-01", "Sales", "Marketing"),
    ("Judy", "McAllister", "1950-08-11", "1994-12-15", "Director", "Technology"),
    ("Paul", "Sturm", "1977-03-11", "2007-08-01", "Developer", "Technology")
;
