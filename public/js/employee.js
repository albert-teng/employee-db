Ext.onReady(() => {
    // Init column model
    let employeeColumnModel = new Ext.grid.ColumnModel({
        columns: [
            {
                id: 'id',
                header: 'ID',
                dataIndex: 'ID',
                fixed: true,
                width: 40
            },
            {
                id: 'firstname',
                header: 'First Name',
                dataIndex: 'FirstName',
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },
            {
                id: 'lastname',
                header: 'Last Name',
                dataIndex: 'LastName',
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },
            {
                id: 'birthdate',
                header: 'Date of Birth',
                renderer: Ext.util.Format.dateRenderer('M d, Y'),
                dataIndex: 'BirthDate',
                editor: new Ext.form.DateField({
                    format: 'M d, Y'
                })
            },
            {
                id: 'startdate',
                header: 'Start Date',
                renderer: Ext.util.Format.dateRenderer('M d, Y'),
                dataIndex: 'StartDate',
                editor: new Ext.form.DateField({
                    format: 'M d, Y'
                })
            },
            {
                id: 'position',
                header: 'Position',
                dataIndex: 'Position',
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },
            {
                id: 'department',
                header: 'Department',
                dataIndex: 'Department',
                editor: new Ext.form.TextField({
                    allowBlank: false
                })
            },
            {
                id: 'status',
                header: 'Status',
                dataIndex: 'Status'
            },
            {
                xtype: 'actioncolumn',
                width: 80,
                fixed: true,
                items: [{
                    icon: '../images/save.gif',
                    handler: (grid, rowIndex, colIndex) => {
                        let employee = employeeDataStore.getAt(rowIndex);

                        // Only make the AJAX request if there are any changes
                        if (employee.modified) {
                            let saveUrl = '/employee/edit/' + employee.data.ID;
                            if (typeof employee.data.ID === 'undefined') {
                                saveUrl = '/employee/add'
                            }

                            Ext.Ajax.request({
                                url: saveUrl,
                                method: 'PUT',
                                params: employee.data,
                                success: () => {
                                    employeeDataStore.reload();
                                },
                                failure: (xhr) => {
                                    let errorMessage = 'Something went wrong. Please try again later.';

                                    if (xhr.status === 400) {
                                        let jsonMessage = JSON.parse(xhr.responseText);

                                        errorMessage = jsonMessage.error;
                                    }

                                    Ext.Msg.show({
                                        title: 'Unable to save changes',
                                        msg: errorMessage,
                                        icon: Ext.MessageBox.ERROR,
                                        buttons: Ext.MessageBox.OK,
                                        minWidth: 500
                                    });
                                }
                            });

                        }
                    }
                }, {
                    icon: '../images/user_edit.png',
                    handler: (grid, rowIndex, colIndex) => {
                        grid.startEditing(rowIndex, 1);
                    }
                }, {
                    icon: '../images/user_delete.png',
                    handler: (grid, rowIndex, colIndex) => {
                        let employee = employeeDataStore.getAt(rowIndex);

                        Ext.Msg.confirm(
                            'Warning',
                            'Are you sure you want to terminate this employee?',
                            (btn) => {
                                if (btn === 'yes') {
                                    Ext.Ajax.request({
                                        url: '/employee/delete/' + employee.data.ID,
                                        method: 'DELETE',
                                        success: () => {
                                            employeeDataStore.reload();
                                        },
                                        failure: (xhr) => {
                                            let errorMessage = 'Something went wrong. Please try again later.';

                                            if (xhr.status === 400) {
                                                let jsonMessage = JSON.parse(xhr.responseText);

                                                errorMessage = jsonMessage.error;
                                            }

                                            Ext.Msg.show({
                                                title: 'Unable to save changes',
                                                msg: errorMessage,
                                                icon: Ext.MessageBox.ERROR,
                                                buttons: Ext.MessageBox.OK,
                                                minWidth: 500
                                            });
                                        }
                                    });
                                }
                            }
                        );
                    }
                }]
            }
        ]
    });

    // Init employee data store
    let employeeDataStore = new Ext.data.JsonStore({
        autoLoad: true,
        autoSave: false,
        fields: [
            {name: 'ID'},
            {name: 'FirstName'},
            {name: 'LastName'},
            {name: 'BirthDate'},
            {name: 'StartDate'},
            {name: 'Position'},
            {name: 'Department'},
            {name: 'Status'}
        ],
        restful: true,
        proxy: new Ext.data.HttpProxy({
            url: '/employee'
        })
    });

    // Init grid panel for employee table
    let employeeTable = new Ext.grid.EditorGridPanel({
        store: employeeDataStore,
        cm: employeeColumnModel,
        height: 300,
        stripeRows: true,
        viewConfig: {
            forceFit: true
        },
        buttons: [{
            text: 'Add Employee',
            handler: () => {
                let Employee = employeeTable.getStore().recordType;

                let newEmployee = new Employee({
                    FirstName: '',
                    LastName: '',
                    BirthDate: '',
                    StartDate: '',
                    Position: '',
                    Department: '',
                    Status: 'Active'
                });

                employeeTable.stopEditing();
                employeeDataStore.add(newEmployee);
                employeeTable.startEditing(employeeDataStore.getCount()-1, 1);
            }
        }],
        buttonAlign: 'left'
    });

    employeeTable.render('employee-list');
});
