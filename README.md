# Employee DB

Simple employee list application

## How to install

Clone this repo and execute the following from within the folder:
```
composer install
```

Once composer has installed all the dependencies, we need to populate the database with some initial data using the following:
```
composer seed-database
```

Now we need to make sure that the page is available in the browser by running the following:
```
composer run --timeout 0 serve
```

The page will be available here: http://0.0.0.0:8080

## Troubleshooting

### Composer failed to seed the database

Try running the following instead:
```
sqlite data/employee.db < data/schema.sql
```
